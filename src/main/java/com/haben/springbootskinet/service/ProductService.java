package com.haben.springbootskinet.service;

import com.haben.springbootskinet.dte.ProductResponse;
import com.haben.springbootskinet.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductService {
    List<ProductResponse> getAllProducts();
    Product getProductById(Long id);

    void saveProduct(Product product);
    void deleteProductById(Long id);

    Page<Product> findPaginated(int pageNo, int pageSize);
}
