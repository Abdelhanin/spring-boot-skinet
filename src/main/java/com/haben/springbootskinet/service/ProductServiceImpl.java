package com.haben.springbootskinet.service;

import com.haben.springbootskinet.dao.ProductRepository;
import com.haben.springbootskinet.dte.ProductResponse;
import com.haben.springbootskinet.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Override
    public List<ProductResponse> getAllProducts() {
        List<Product> produstList = productRepository.findAll();
        List<ProductResponse> productResponses = new ArrayList<ProductResponse>();

//        produstList.forEach(product -> {
//            ProductResponse productResponse = ProductResponse
//                    .builder()
//                    .id(product.getId())
//                    .name(product.getName())
//                    .price(product.getPrice())
//                    .description(product.getDescription())
//                    .pictureUrl(product.getPictureUrl())
//                    .productTypeId(product.getProductType().getId())
//                    .productBrandId(product.getProductBrand().getId()).build();
//            productResponses.add(productResponse);
//        });
//        System.out.println("productResponses" + productResponses);
//        System.out.println("produstList" + produstList);
        return productResponses;
    }

    @Override
    public Product getProductById(Long id) {
        Optional<Product> optional = productRepository.findById(id);
        Product product = null;

        if (optional.isPresent()) {
            product = optional.get();
        } else {
            throw new RuntimeException("Product not found for id :: " + id);
        }
        return product;
    }

    @Override
    public void saveProduct(Product product) {
        productRepository.save(product);
    }

    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<Product> findPaginated(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        return this.productRepository.findAll(pageable);
    }
}
