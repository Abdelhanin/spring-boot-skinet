package com.haben.springbootskinet.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.concurrent.TimeUnit;

import static com.haben.springbootskinet.security.ApplicationUserRole.ADMIN;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true) // when we use the authorisation in controller we need this
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
//    @Autowired
//    UserDetailsService userDetailsService;

    public final PasswordEncoder passwordEncoder;

    @Autowired
    public SecurityConfiguration(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http    /*.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                .and()*/
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/api/product/**").hasRole(ADMIN.name())
//                .antMatchers(HttpMethod.DELETE,"/management/api/v1/**").hasAnyAuthority(STUDENT_WRITE.name(), COURSE_WRITE.getPermission())
//                .antMatchers(HttpMethod.PUT,"/management/api/v1/**").hasAnyAuthority(STUDENT_WRITE.name(), COURSE_WRITE.getPermission())
//                .antMatchers(HttpMethod.POST,"/management/api/v1/**").hasAnyAuthority(STUDENT_WRITE.name(), COURSE_WRITE.getPermission())
//                .antMatchers(HttpMethod.GET,"/management/api/v1/**").hasAnyRole(ADMIN.name(), STUDENT.name())
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
               /* .loginPage("/login").permitAll()
                .defaultSuccessUrl("/courses", true);*/
      /*              .passwordParameter("password") // if we want change the default name of input value
                    .usernameParameter("username")*/
                .and()
                .rememberMe() // default to 2 weeks
                    .tokenValiditySeconds((int) TimeUnit.DAYS.toSeconds(21))// this how extend our session with remember me
                    .key("somethingverysecure")
                    //.rememberMeParameter("remember-me")
                .and()
                .logout()
                    .logoutUrl("/logout")
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET")) // when enable csrf() we should delete this line
                    .clearAuthentication(true)
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID", "remember-me")
                    .logoutSuccessUrl("/login");//when we logout
    }

    // bean it will instantiate for us
    // 'UserDetailsService', ho retreive users from our database
    /*@Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails haben = User
                .builder()
                .username("hanine")
                .password(passwordEncoder.encode("123456"))
                //.roles("ADMIN") // ROLE_ADMIN
                //.roles(ADMIN.name())
                .authorities(ADMIN.getGrantedAuthorities())
                .build();

        UserDetails sofia = User
                .builder()
                .username("sofia")
                .password(passwordEncoder.encode("123456"))
                //.roles("CUSTOMER") // ROLE_ADMIN
                //.roles(CUSTOMER.name())
                .authorities(CUSTOMER.getGrantedAuthorities())
                .build();

        UserDetails anis = User
                .builder()
                .username("anis")
                .password(passwordEncoder.encode("123456"))
                //.roles("CUSTOMER") // ROLE_ADMIN
                //.roles(ApplicationUserRole.STUDENT.name())
                .authorities(STUDENT.getGrantedAuthorities())
                .build();

        UserDetails naim = User
                .builder()
                .username("naim")
                .password(passwordEncoder.encode("123456"))
                //.roles("CUSTOMER") // ROLE_ADMIN
                //.roles(ApplicationUserRole.STUDENT.name())
                .authorities(STUDENT.getGrantedAuthorities())
                .build();

        return new InMemoryUserDetailsManager(haben, sofia, anis, naim);
    }*/
}
