package com.haben.springbootskinet.security;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository("fake") // repository means this class need to be instantiated
public class FakeApplicationUserDaoService implements ApplicationUserDAO {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public FakeApplicationUserDaoService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()))
                .findFirst();
    }

    public List<ApplicationUser> getApplicationUsers() {
        List<ApplicationUser> applicationUsers = Lists.newArrayList(
            new ApplicationUser(
                    ApplicationUserRole.ADMIN.getGrantedAuthorities(),
                    passwordEncoder.encode("123456"),
                    "hanine",
                    true,
                    true,
                    true,
                    true
                    ),
            new ApplicationUser(
                    ApplicationUserRole.CUSTOMER.getGrantedAuthorities(),
                    passwordEncoder.encode("123456"),
                    "sofia",
                    true,
                    true,
                    true,
                    true
            ),
            new ApplicationUser(
                    ApplicationUserRole.STUDENT.getGrantedAuthorities(),
                    passwordEncoder.encode("123456"),
                    "anis",
                    true,
                    true,
                    true,
                    true
            )
        );
        return applicationUsers;
    }
}
