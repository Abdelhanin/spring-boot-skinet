package com.haben.springbootskinet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSkinetApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSkinetApplication.class, args);
	}

}
