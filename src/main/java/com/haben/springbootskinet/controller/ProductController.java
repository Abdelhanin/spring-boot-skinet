package com.haben.springbootskinet.controller;

import com.haben.springbootskinet.dao.ProductRepository;
import com.haben.springbootskinet.dte.ProductResponse;
import com.haben.springbootskinet.entity.Product;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    //private final ProductService productService;
    @Autowired
    private ProductRepository productRepository;




//    // display list of Product
//    @GetMapping(path = "/products")
//    public List<ProductResponse> getAllProduct() {
//        List<ProductResponse> productResponses;
//        productResponses = this.productService.getAllProducts();
//
//        return productResponses;
//    }


    private Sort.Direction getSortDirection(String direction) {
        if (direction.equals("asc")) {
            return Sort.Direction.ASC;
        } else if (direction.equals("desc")) {
            return Sort.Direction.DESC;
        }

        return Sort.Direction.ASC;
    }


    @GetMapping("/all-products")
    public @ResponseBody ResponseEntity<List<ProductResponse>> getAllProducts(@RequestParam(defaultValue = "id,desc") String[] sort) {

        try {
            List<Order> orders = new ArrayList<Order>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<Product> produstList = productRepository.findAll(Sort.by(orders));
            List<ProductResponse> productResponses = new ArrayList<ProductResponse>();

            produstList.forEach(product -> {
                ModelMapper modelMapper = new ModelMapper();
                ProductResponse productResponse = new ProductResponse();
                productResponse = modelMapper.map(product, ProductResponse.class);
                productResponses.add(productResponse);
        });

            if (productResponses.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<List<ProductResponse>>(productResponses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/products")
    public ResponseEntity<Map<String, Object>> getAllProductsPage(
            @RequestParam(required = false) String name,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "id,desc") String[] sort) {

        try {
            List<Order> orders = new ArrayList<Order>();

            if (sort[0].contains(",")) {
                // will sort more than 2 fields
                // sortOrder="field, direction"
                for (String sortOrder : sort) {
                    String[] _sort = sortOrder.split(",");
                    orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
                }
            } else {
                // sort=[field, direction]
                orders.add(new Order(getSortDirection(sort[1]), sort[0]));
            }

            List<Product> products = new ArrayList<Product>();
            Pageable pagingSort = PageRequest.of(page - 1, size, Sort.by(orders));

            Page<Product> pageTuts;
            if (name == null)
                pageTuts = productRepository.findAll(pagingSort);
            else
                pageTuts = productRepository.findByNameContaining(name, pagingSort);

            products = pageTuts.getContent();


            List<ProductResponse> productResponses = new ArrayList<ProductResponse>();

            products.forEach(product -> {
                ModelMapper modelMapper = new ModelMapper();
                ProductResponse productResponse = new ProductResponse();
                productResponse = modelMapper.map(product, ProductResponse.class);
                productResponses.add(productResponse);
            });


            Map<String, Object> response = new HashMap<>();
            response.put("products", productResponses);
            response.put("currentPage", pageTuts.getNumber() + 1);
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<ProductResponse> getTutorialById(@PathVariable("id") long id) {
        Optional<Product> productData = productRepository.findById(id);


        if (productData.isPresent()) {
            ProductResponse productResponse = new ProductResponse();
            ModelMapper modelMapper = new ModelMapper();
            productResponse = modelMapper.map(productData.get(), ProductResponse.class);
            return new ResponseEntity<>(productResponse, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
