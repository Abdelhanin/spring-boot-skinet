package com.haben.springbootskinet.controller;

import com.haben.springbootskinet.entity.Student;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/management/api/v1/students")
public class StudentManagementController {
    private static final List<Student> STUDENTS = Arrays.asList(
            new Student(1, "Hanine Benhammou"),
            new Student(2, "Anis Benhammou"),
            new Student(3, "Naim Benhammou")
    );

    // hasRole('ROLE_') , hasAnyRole('ROLE_') , hasAuthority, hasAnyAuthority
    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_STUDENT')")
    public List<Student> getStudents() {
        return STUDENTS;
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('course:write', 'student:write')")
    public void registerNewStudent(@RequestBody Student student) {
        System.out.println(student);
    }

    @DeleteMapping(path = "{studentId}")
    @PreAuthorize("hasAnyAuthority('course:write', 'student:write')")
    public void deleteStudent(@PathVariable Integer studentId) {
        System.out.println("deleted student");
    }

    @PutMapping(path = "{studentId}")
    @PreAuthorize("hasAnyAuthority('course:write', 'student:write')")
    public void updateStudent(@PathVariable Integer studentId, @RequestBody Student student) {
        System.out.println("updated student with id" + studentId);
    }

    @GetMapping(path = "{studentId}")
    @PreAuthorize("hasAnyAuthority('course:write', 'student:write')")
    public Student getStudentById(@PathVariable Integer studentId) {
        return STUDENTS.get(studentId);
    }
}
