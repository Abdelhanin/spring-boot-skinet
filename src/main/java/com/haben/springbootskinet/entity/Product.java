package com.haben.springbootskinet.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="product")
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "picture_url")
    private String pictureUrl;
    @ManyToOne
    @JoinColumn(name = "product_type_id", nullable = false)
    private ProductType productType;
    @ManyToOne
    @JoinColumn(name = "product_brand_id", nullable = false)
    private ProductBrand productBrand;

    public Product() {

    }

    public Product(String name, String description, BigDecimal price, String pictureUrl, ProductType productType, ProductBrand productBrand) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.pictureUrl = pictureUrl;
        this.productType = productType;
        this.productBrand = productBrand;
    }
}
