package com.haben.springbootskinet.dte;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ProductResponse implements Serializable {
    private Long id;
    private String name;
    private BigDecimal price;
    private String description;
    private String pictureUrl;
    private Long productTypeId;
    private Long productBrandId;

    public ProductResponse() {}

    public ProductResponse(Long id, String name, BigDecimal price, String description, String pictureUrl, Long productTypeId, Long productBrandId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.pictureUrl = pictureUrl;
        this.productTypeId = productTypeId;
        this.productBrandId = productBrandId;
    }
}
