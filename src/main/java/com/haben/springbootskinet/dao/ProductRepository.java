package com.haben.springbootskinet.dao;

import com.haben.springbootskinet.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RepositoryRestResource
public interface ProductRepository  extends JpaRepository<Product, Long> {


//    Page<Product> findByPublished(boolean published, Pageable pageable);

    Page<Product> findByNameContaining(String name, Pageable pageable);

    List<Product> findByNameContaining(String name, Sort sort);


//    Page<Product> findByTitleContaining(String title, Pageable pageable);
//
//    List<Product> findByTitleContaining(String title, Sort sort);


    // search by name
//    Page<Product> findByNameContaining(@RequestParam("name") String name, Pageable pageable);
    Page<Product> findByNameContainingOrderByPriceAsc(@RequestParam("name") String name, Pageable pageable);

    // sort by product name
    Page<Product> findByOrderByNameAsc(Pageable pageable);
    Page<Product> findByOrderByNameDesc(Pageable pageable);

    // sort by product price
    Page<Product> findByOrderByPriceAsc(Pageable pageable);
    Page<Product> findByOrderByPriceDesc(Pageable pageable);


    // find by product type and sorting by price or by name
    Page<Product> findByProductTypeId(@RequestParam("id") Long id, Pageable pageable);

    Page<Product> findByProductTypeIdOrderByPriceAsc(@RequestParam("id") Long id, Pageable pageable);
    Page<Product> findByProductTypeIdOrderByPriceDesc(@RequestParam("id") Long id, Pageable pageable);

    Page<Product> findByProductTypeIdOrderByNameAsc(@RequestParam("id") Long id, Pageable pageable);
    Page<Product> findByProductTypeIdOrderByNameDesc(@RequestParam("id") Long id, Pageable pageable);


    // find by product brand and sorting by price or by name
    Page<Product> findByProductBrandId(@RequestParam("id") Long id, Pageable pageable);

    Page<Product> findByProductBrandIdOrderByPriceAsc(@RequestParam("id") Long id, Pageable pageable);
    Page<Product> findByProductBrandIdOrderByPriceDesc(@RequestParam("id") Long id, Pageable pageable);

    Page<Product> findByProductBrandIdOrderByNameAsc(@RequestParam("id") Long id, Pageable pageable);
    Page<Product> findByProductBrandIdOrderByNameDesc(@RequestParam("id") Long id, Pageable pageable);

    //Find product by brandId and TypeId
    Page<Product> findByProductBrandIdAndProductTypeId(@RequestParam("brandId") Long brandId, @RequestParam("typeId") Long typeId,Pageable pageable);
    Page<Product> findByProductBrandIdAndProductTypeIdOrderByPriceAsc(@RequestParam("brandId") Long brandId, @RequestParam("typeId") Long typeId,Pageable pageable);
    Page<Product> findByProductBrandIdAndProductTypeIdOrderByPriceDesc(@RequestParam("brandId") Long brandId, @RequestParam("typeId") Long typeId,Pageable pageable);
}
