package com.haben.springbootskinet.dao;

import com.haben.springbootskinet.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTypeRepository extends JpaRepository<ProductType, Long> {
}
