package com.haben.springbootskinet.dao;

import com.haben.springbootskinet.entity.ProductBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductBrandRepository extends JpaRepository<ProductBrand, Long> {
}
